import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CostRoutes } from './cost.routing';

@NgModule({
  imports: [
    CommonModule, CostRoutes
  ],
  declarations: []
})
export class CostModule { }
