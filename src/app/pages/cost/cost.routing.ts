import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../account/guards/auth.guard';
import { BranchComponent } from './branch/branch.component';
import { DepartmentComponent } from './department/department.component';
import { LocationComponent } from './location/location.component';

const routes: Routes = [
  { path: 'location', component: LocationComponent,canActivate: [AuthGuard]},
  { path: 'branch', component: BranchComponent,canActivate: [AuthGuard]},
  { path: 'department', component: DepartmentComponent,canActivate: [AuthGuard]},
  { path: 'branch', component: BranchComponent,canActivate: [AuthGuard]},
  { path: 'branch', component: BranchComponent,canActivate: [AuthGuard]},
];

export const CostRoutes = RouterModule.forChild(routes);
