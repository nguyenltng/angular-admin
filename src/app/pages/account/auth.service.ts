import { Injectable } from '@angular/core';
import { of, switchMap } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { LogService } from 'src/app/services/log.service';
import { environment } from 'src/environments/environment';
import { UserLogin } from './UserLogin';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    _authenticated:boolean = false;
    UrlApi : string = environment.UrlApi || "";

    constructor(private logService: LogService,private apiService: ApiService) {
     }
    /**
     * 
     * @param email email login
     * @param pass  password                
     */
    /**
     * Setter & getter for access token
     */
     set userLogin(user: any)
     {
        const struser:string = JSON.stringify(user);
        localStorage.setItem('user', struser);
     }
 
     get userLogin(): UserLogin
     {
        const struser:string = localStorage.getItem('user') ?? '';
        if (struser == '') {
            return new UserLogin();
        }
        return JSON.parse(struser);
     }
    
    login(email: string,pass: string) {
        return this.apiService.get(this.UrlApi +"auth/login",{username:email,password:pass}).pipe(

            switchMap((response:any)=>{
                console.log(response);
                if (response.token.length > 0) {
                    this.userLogin = {id:response.id,username:email,password:pass,token:response.token,refreshToken:response.refreshToken};
                    this._authenticated = true;
                    return of(true)
                }
                return of(false);
            }),
        )
    }
    logout(){
        this.userLogin = {};
        this._authenticated = false;
        localStorage.removeItem('user');
    }
}
