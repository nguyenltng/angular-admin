import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LogService } from 'src/app/services/log.service';
import { AuthService } from '../auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

    // Login Form
    loginForm!: FormGroup;
    submitted = false;
    fieldTextType!: boolean;
    error = '';
    returnUrl!: string;
    toast = false;
    constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private authService: AuthService,private logService:LogService) {
        // redirect to home if already logged in
        if (authService.userLogin?.username) {
            logService.log("Đã login");
            this.router.navigate(['/']);
        }
    }
    ngOnInit() {
        /**
        * Form Validatyion
        */
        this.loginForm = this.formBuilder.group({
            email: ['nguyenltng', [Validators.required]],
            password: ['123', [Validators.required]],
        });
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        } else {
            this.authService.login(this.f['email'].value, this.f['password'].value).subscribe((data) => {
                console.log(data);
                if (data) {
                    this.router.navigate(['/' + this.returnUrl]);
                }else{
                    console.log(this.toast);
                    this.toast = true;
                }
            }, (err) => {
                console.log(err);
                this.toast = true;
            })
        }
    }
    /**
     * Password Hide/Show
     */
    toggleFieldTextType() {
        this.fieldTextType = !this.fieldTextType;
    }
}

