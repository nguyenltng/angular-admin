export class UserLogin {
    id?: number;
    username?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    token?: string;
    refreshToken?: string;
    email?: string;
}