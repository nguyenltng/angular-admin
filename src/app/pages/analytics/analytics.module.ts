import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyticsRoutes } from './analytics.routing';
import { ComponentModule } from 'src/app/shared/component/component.module';
import { BranchComponent } from './branch/branch.component';
import {  NgbDropdown, NgbDropdownModule, NgbHighlight, NgbNavModule, NgbPagination, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';
import { CountToModule } from 'angular-count-to';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';
import { DataTablesModule } from 'angular-datatables';
import flatpickr from 'flatpickr';
// import { Vietnamese } from 'flatpickr/dist/l10n/vn';
// export function flatpickrFactory() {
//   flatpickr.localize(Vietnamese);
//   return flatpickr;
// }
// Load Icons
import { defineLordIconElement } from 'lord-icon-element';
import lottie from 'lottie-web';
import { LocationComponent } from './location/location.component';
import { TeamComponent } from './team/team.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AnalyticsRoutes,
    ComponentModule,
    NgbNavModule,
    NgbDropdownModule,
    NgApexchartsModule,
    CountToModule,
    NgbPaginationModule,
    FormsModule,
    NgbTypeaheadModule,
    FlatpickrModule.forRoot(),
    DataTablesModule,
  ],
  declarations: [BranchComponent,LocationComponent,TeamComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class AnalyticsModule { 
  constructor() {
    defineLordIconElement(lottie.loadAnimation);
  }
}
