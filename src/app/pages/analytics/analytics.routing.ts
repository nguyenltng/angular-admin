import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../account/guards/auth.guard';
import { BranchComponent } from './branch/branch.component';
// import { CustomerComponent } from './customer/customer.component';
import { LocationComponent } from './location/location.component';
import { PartnerComponent } from './partner/partner.component';
import { ProductComponent } from './product/product.component';
import { TeamComponent } from './team/team.component';
import { TimeComponent } from './time/time.component';

const routes: Routes = [
  { path:'branch', component: BranchComponent,canActivate: [AuthGuard]},
  { path:'location', component: LocationComponent,canActivate: [AuthGuard]},
  { path:'team', component: TeamComponent,canActivate: [AuthGuard]},
  { path:'product', component: ProductComponent,canActivate: [AuthGuard]},
  // { path:'customer', component: CustomerComponent,canActivate: [AuthGuard]},
  { path:'partner', component: PartnerComponent,canActivate: [AuthGuard]},
  { path:'time', component: TimeComponent,canActivate: [AuthGuard]},
];

export const AnalyticsRoutes = RouterModule.forChild(routes);
