import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-examplecomponent',
    templateUrl: './examplecomponent.component.html',
    styleUrls: ['./examplecomponent.component.css']
})
export class ExamplecomponentComponent implements OnInit {
    breadCrumbItems!: Array<{}>;
    DataCount1!: any;
    DataCount2!: any;
    DataCount3!: any;
    DataCount4!: any;
    DataCount5!: any;
    DataCount2Example = [{
        title: 'Users',
        value: 28.05,
        icon: 'users',
        persantage: '16.24',
        profit: 'up',
        subtitlelast: ' vs. previous month'
    }, {
        title: 'Sessions',
        value: 97.66,
        icon: 'activity',
        persantage: '3.96',
        profit: 'down',
        subtitlelast: ' vs. previous month'
    }, {
        title: 'Avg. Visit Duration',
        value: 3.40,
        icon: 'clock',
        persantage: '0.24',
        profit: 'down',
        subtitlelast: ' vs. previous month'
    }, {
        title: 'Bounce Rate',
        value: 33.48,
        icon: 'external-link',
        persantage: '7.05',
        profit: 'up',
        subtitlelast: ' vs. previous month'
    }
    ];
    DataCount1Exmplate = [{
        title: 'CAMPAIGN SENT',
        value: 197,
        icon: 'ri-space-ship-line',
        profit: 'up'
    }, {
        title: 'ANNUAL PROFIT',
        value: 489.4,
        icon: 'ri-exchange-dollar-line',
        profit: 'up'
    }, {
        title: 'LEAD COVERSATION',
        value: 32.89,
        icon: 'ri-pulse-line',
        profit: 'down'
    }, {
        title: 'DAILY AVERAGE INCOME',
        value: 1596.5,
        icon: 'ri-trophy-line',
        profit: 'up'
    }, {
        title: 'ANNUAL DEALS',
        value: 2659,
        icon: 'ri-service-line',
        profit: 'down'
    }
    ];
    DataCount3Example = [{
        title: 'Users',
        value: 28.05,
        icon: 'ri-money-dollar-circle-fill',
        persantage: '16.24',
        profit: 'up',
    }, {
        title: 'Sessions',
        value: 97.66,
        icon: 'ri-money-dollar-circle-fill',
        persantage: '3.96',
        profit: 'down',
    }, {
        title: 'Avg. Visit Duration',
        value: 3.40,
        icon: 'ri-money-dollar-circle-fill',
        persantage: '0.24',
        profit: 'down',
    }, {
        title: 'Bounce Rate',
        value: 33.48,
        icon: 'ri-money-dollar-circle-fill',
        persantage: '7.05',
        profit: 'up',
    }
    ];
    DataCount4Example = [{
        title: 'TOTAL EARNINGS',
        value: 559.25,
        icon: 'bx-dollar-circle',
        persantage: '16.24',
        profit: 'up',
        titlelink: "CHỖ NÀY ĐỂ LINK",
        link: "https://kasco.vn",
    }, {
        title: 'ORDERS',
        value: 36894,
        icon: 'bx-shopping-bag',
        persantage: '3.57',
        profit: 'down',
        titlelink: "CHỖ NÀY ĐỂ LINK",
        link: "https://kasco.vn",
    }, {
        title: 'CUSTOMERS',
        value: 183.35,
        icon: 'bx-user-circle',
        persantage: '29.08',
        profit: 'up',
        titlelink: "CHỖ NÀY ĐỂ LINK",
        link: "https://kasco.vn",
    }, {
        title: 'MY BALANCE',
        value: 165.89,
        icon: 'bx-wallet',
        persantage: '0.00',
        profit: 'equal',
        titlelink: "CHỖ NÀY ĐỂ LINK",
        link: "https://kasco.vn",
    }
    ];
    DataCount5Example = [{
        title: 'ACTIVE PROJECTS',
        value: 825,
        icon: 'briefcase',
        persantage: '5.02',
        profit: 'down',
        month: 'Projects this month'
      }, {
        title: 'NEW LEADS',
        value: 7522,
        icon: 'award',
        persantage: '3.58',
        profit: 'up',
        month: 'Leads this month'
      }, {
        title: 'TOTAL HOURS',
        value: 168.40,
        icon: 'clock',
        persantage: '10.35 ',
        profit: 'down',
        month: 'Work this month'
      }
    ];
    constructor() { }

    ngOnInit() {
        this.breadCrumbItems = [
            { label: 'Example', active: true },
        ]
        this.FnLoadDataExample();
    }
    private FnLoadDataExample() {
        this.DataCount1 = this.DataCount1Exmplate;
        this.DataCount2 = this.DataCount2Example;
        this.DataCount3 = this.DataCount3Example;
        this.DataCount4 = this.DataCount4Example;
        this.DataCount5 = this.DataCount5Example;
    }
}
