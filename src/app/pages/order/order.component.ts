import { Component, OnInit } from '@angular/core';
import { Product, TopSelling, TableRows, Employee } from './table-data';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {

  breadCrumbItems!: Array<{}>;

  topSelling: Product[];

  trow: TableRows[];
  constructor() {  this.topSelling = TopSelling;

    this.trow = Employee;}

  ngOnInit() {
    /**
    * BreadCrumb
    */
    this.breadCrumbItems = [
      { label: 'Order' },
      // { label: 'HRM', active: true }
    ];
   
    // this.FnLoadData();
    // this._salesForecastChart('["--vz-primary", "--vz-success", "--vz-warning"]');
    // this._DealTypeChart('["--vz-warning", "--vz-danger", "--vz-success"]');
    // this._splineAreaChart('["--vz-success", "--vz-danger"]');
    // this._basicBarChart('["--vz-info", "--vz-info", "--vz-info", "--vz-info", "--vz-danger", "--vz-info", "--vz-info", "--vz-info", "--vz-info", "--vz-info"]');
    // this._basicColumnChart('["--vz-success", "--vz-gray-300"]');
    // this._OverviewChart('["--vz-primary", "--vz-warning", "--vz-success"]');
    // this._status7('["--vz-success", "--vz-primary", "--vz-warning", "--vz-danger"]');


  }

}
