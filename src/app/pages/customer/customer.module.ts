import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerComponent } from './customer.component';
import { ComponentModule } from 'src/app/shared/component/component.module';
import { CustomerRoutes } from './customer.routing';



@NgModule({
  declarations: [
    CustomerComponent
  ],
  imports: [
    CommonModule,
    CustomerRoutes,
    ComponentModule
  ]
})
export class CustomerModule { }
