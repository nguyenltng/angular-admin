import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  breadCrumbItems!: Array<{}>;
  
  constructor() { }

  ngOnInit(): void {
    this.breadCrumbItems = [
      { label: 'Khách hàng'},
      { label: 'Danh sách', active: true}
    ]
  }

}
