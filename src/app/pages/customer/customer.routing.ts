import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../account/guards/auth.guard';
import { CustomerComponent } from './customer.component';

const routes: Routes = [
  { path: '', component: CustomerComponent ,canActivate: [AuthGuard]},
];

export const CustomerRoutes = RouterModule.forChild(routes);
