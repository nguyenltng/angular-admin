import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConnectionComponent } from './connection/connection.component';
import { SettingRoutes } from './settings.routing';
import { ComponentModule } from 'src/app/shared/component/component.module';
import { BreadcrumbsComponent } from 'src/app/shared/component/breadcrumbs/breadcrumbs.component';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    ConnectionComponent,
  ],
  imports: [
    CommonModule,
    SettingRoutes,
    ComponentModule,
    NgbNavModule,
    FormsModule,
    ReactiveFormsModule,
    // ConnectionComponent
  ],
})
export class SettingsModule { }
