import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { data } from 'jquery';
import { Connection } from 'src/app/model/connection.model';
import { ConnectionService } from 'src/app/services/connection.service';
import { PROVIDERS } from 'src/constants/app.constants';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.scss'],
})
export class ConnectionComponent implements OnInit {
  breadCrumbItems!: Array<{}>;
  misaForm!: FormGroup;
  haravanForm!: FormGroup;
  active = 1;
  submitted = false;
  connectionHaravan!: Connection;
  connectionMisa!: Connection;

  constructor(private formBuilder: FormBuilder, private connectionService: ConnectionService) {
  }

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Cài đặt' },
      { label: 'Kết nối', active: true }
    ];
    this.fetchData();
    this.validate();
  }

  // convenience getter for easy access to form fields
  get fMisa() { return this.misaForm.controls; }
  get fHaravan() { return this.haravanForm.controls; }


  RefeshWidth() {
    window.dispatchEvent(new Event('resize'));
  }
  onSubmitFormHaravan() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.haravanForm.invalid) {
      return;
    } else {
      let payload: Connection = this.haravanForm.value;
      payload.provider = 'Haravan';

      if(!this.connectionHaravan){
        this.connectionService.addConnection(payload).subscribe((data) => {
          if (data) {
            console.log(data);
          } else {
            console.log(data);
          }
        }, (err) => {
          console.log(err);
        })
      }else{
        this.connectionService.updateConnection(this.connectionHaravan.id?? 0 , payload).subscribe((data) =>{
          if(data){
            console.log(data);
            console.log("updated");
          }else{
            console.log("Fail");
          }
        })
      }
     
    }
  }
  onSubmitFormMisa() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.misaForm.invalid) {
      return;
    } else {
      console.log('Submitted Misa')
    }
  }

  validate(){
    this.misaForm = this.formBuilder.group({
      Username: [this.connectionMisa?.username, [Validators.required]],
      Password: [this.connectionMisa?.password, Validators.required],
      ClientID: [this.connectionMisa?.clientID, Validators.required],
      ClientSecret: [this.connectionMisa?.clientSecret, Validators.required],
      PrivateToken: [this.connectionMisa?.privateToken, Validators.required]
    });
      this.haravanForm = this.formBuilder.group({
      Username: [this.connectionHaravan?.username, [Validators.required]],
      Password: [this.connectionHaravan?.password, Validators.required],
      ClientID: [this.connectionHaravan?.clientID, Validators.required],
      ClientSecret: [this.connectionHaravan?.clientSecret, Validators.required],
      PrivateToken: [this.connectionHaravan?.privateToken, Validators.required]
    });
  }
  fetchData() {
    this.connectionService.getAllConnections().subscribe((data) => {
      if (data) {
        data.forEach((element: any) => {
          if (element.provider == PROVIDERS.haravan) {
            this.connectionHaravan = element;
          } else if (element.provider == PROVIDERS.misa) {
            this.connectionMisa = element;
          }
        });
        this.misaForm.patchValue({
          Username: this.connectionMisa?.username,
          Password: this.connectionMisa?.password,
          ClientID: this.connectionMisa?.clientID,
          ClientSecret: this.connectionMisa?.clientSecret,
          PrivateToken: this.connectionMisa?.privateToken,
        });
          this.haravanForm.patchValue({
          Username: this.connectionHaravan?.username,
          Password: this.connectionHaravan?.password, 
          ClientID: this.connectionHaravan?.clientID, 
          ClientSecret: this.connectionHaravan?.clientSecret,
          PrivateToken: this.connectionHaravan?.privateToken, 
        });
      }
    })
    return;
  }
}
