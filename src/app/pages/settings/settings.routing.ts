import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../account/guards/auth.guard';
import { ConnectionComponent } from './connection/connection.component';

const routes: Routes = [
  { path: 'connection', component: ConnectionComponent,canActivate: [AuthGuard]},
];

export const SettingRoutes = RouterModule.forChild(routes);
