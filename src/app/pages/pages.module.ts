import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutes } from './pages.routing';
import { ExamplecomponentComponent } from './examplecomponent/examplecomponent.component';
import { ComponentModule } from '../shared/component/component.module';
import { NgApexchartsModule } from 'ng-apexcharts';
import { OrderComponent } from './order/order.component';


@NgModule({
  imports: [
    CommonModule,
    PagesRoutes,
    ComponentModule,
    
  ],
  declarations: [ExamplecomponentComponent, OrderComponent],

})
export class PagesModule { }
