import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './account/guards/auth.guard';
import { GeneralComponent } from './dashboards/general/general.component';
import { ExamplecomponentComponent } from './examplecomponent/examplecomponent.component';
import { OrderComponent } from './order/order.component';

const routes: Routes = [
  {
    path: "",
    component: GeneralComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'example',
    component: ExamplecomponentComponent,
  },
  {
    path: 'dashboard', loadChildren: () => import('./dashboards/dashboards.module').then(m => m.DashboardsModule), canActivate: [AuthGuard]
  },
  {
    path: 'analytics', loadChildren: () => import('./analytics/analytics.module').then(m => m.AnalyticsModule), canActivate: [AuthGuard]
  },
  {
    path: 'cost', loadChildren: () => import('./cost/cost.module').then(m => m.CostModule), canActivate: [AuthGuard]
  },
  {
    path: 'profit', loadChildren: () => import('./profit/profit.module').then(m => m.ProfitModule), canActivate: [AuthGuard]
  },
  {
    path: 'order', 
    component: OrderComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'setting', loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule), canActivate: [AuthGuard]
  },
  {
    path: 'customer', loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule), canActivate: [AuthGuard]
  },
];

export const PagesRoutes = RouterModule.forChild(routes);
