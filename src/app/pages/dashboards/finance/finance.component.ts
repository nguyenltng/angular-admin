import { Component, OnInit } from '@angular/core';
import { Common } from 'src/app/shared/common';

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.css']
})
export class FinanceComponent implements OnInit {

  common:Common = new Common();  
  breadCrumbItems!: Array<{}>;
  dataTotal!: any;
  salesForecastChart: any;
  DealTypeChart:any;
  splineAreaChart: any;
  basicBarChart: any;
  basicColumnChart: any;
  OverviewChart: any;
  status7: any;

  constructor() { }

  ngOnInit() {
      /**
      * BreadCrumb
      */
      this.breadCrumbItems = [
          { label: 'Dashboards' },
          { label: 'Tài chính', active: true }
      ];
      this.FnLoadData();
      this._salesForecastChart('["--vz-primary", "--vz-success", "--vz-warning"]');
      this._DealTypeChart('["--vz-warning", "--vz-danger", "--vz-success"]');
      this._splineAreaChart('["--vz-success", "--vz-danger"]');
      this._basicBarChart('["--vz-info", "--vz-info", "--vz-info", "--vz-info", "--vz-danger", "--vz-info", "--vz-info", "--vz-info", "--vz-info", "--vz-info"]');
      this._basicColumnChart('["--vz-success", "--vz-gray-300"]');
      this._OverviewChart('["--vz-primary", "--vz-warning", "--vz-success"]');
      this._status7('["--vz-success", "--vz-primary", "--vz-warning", "--vz-danger"]');


  }
  private _OverviewChart(colors:any) {
      colors = this.common.getChartColorsArray(colors);
      this.OverviewChart = {
        series: [{
            name: 'Number of Projects',
            type: 'bar',
            data: [34, 65, 46, 68, 49, 61, 42, 44, 78, 52, 63, 67]
        }, {
            name: 'Revenue',
            type: 'area',
            data: [89.25, 98.58, 68.74, 108.87, 77.54, 84.03, 51.24, 28.57, 92.57, 42.36, 88.51, 36.57]
        }, {
            name: 'Active Projects',
            type: 'bar',
            data: [8, 12, 7, 17, 21, 11, 5, 9, 7, 29, 12, 35]
        }],
        chart: {
            height: 374,
            type: 'line',
            toolbar: {
                show: false,
            }
        },
        stroke: {
            curve: 'smooth',
            dashArray: [0, 3, 0],
            width: [0,1, 0],
        },
        fill: {
            opacity: [1, 0.1, 1]
        },
        markers: {
            size: [0, 4, 0],
            strokeWidth: 2,
            hover: {
                size: 4,
            }
        },
        xaxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            axisTicks: {
                show: false
            },
            axisBorder: {
                show: false
            }
        },
        grid: {
            show: true,
            xaxis: {
                lines: {
                    show: true,
                }
            },
            yaxis: {
                lines: {
                    show: false,
                }
            },
            padding: {
                top: 0,
                right: -2,
                bottom: 15,
                left: 10
            },
        },
        legend: {
            show: true,
            horizontalAlign: 'center',
            offsetX: 0,
            offsetY: -5,
            markers: {
                width: 9,
                height: 9,
                radius: 6,
            },
            itemMargin: {
                horizontal: 10,
                vertical: 0
            },
        },
        plotOptions: {
            bar: {
                columnWidth: '30%',
                barHeight: '70%'
            }
        },
        colors: colors,
        tooltip: {
        shared: true,
        y: [{
            formatter: function (y:any) {
              if(typeof y !== "undefined") {
                return  y.toFixed(0);
              }
              return y;
              
            }
          }, {
            formatter: function (y:any) {
              if(typeof y !== "undefined") {
                return   "$" + y.toFixed(2) + "k";
              }
              return y;
              
            }
          }, {
            formatter: function (y:any) {
              if(typeof y !== "undefined") {
                return y.toFixed(0);
              }
              return y;
              
            }
          }]
        }
      };
    }
  
    /**
   *  Status7
   */
    private _status7(colors:any) {
      colors = this.common.getChartColorsArray(colors);
      this.status7 = {
        series: [125, 42, 58, 89],
        labels: ["Completed", "In Progress", "Yet to Start", "Cancelled"],
        chart: {
            type: "donut",
            height: 230,
        },
        plotOptions: {
            pie: {
                offsetX: 0,
                offsetY: 0,
                donut: {
                    size: "90%",
                    labels: {
                        show: false,
                    }
                },
            },
        },
        dataLabels: {
            enabled: false,
        },
        legend: {
            show: false,
        },
        stroke: {
            lineCap: "round",
            width: 0
        },
        colors: colors
      };
  }
  private _basicColumnChart(colors:any) {
      colors = this.common.getChartColorsArray(colors);
      this.basicColumnChart = {
        series: [{
            name: 'Last Year',
            data: [25.3, 12.5, 20.2, 18.5, 40.4, 25.4, 15.8, 22.3, 19.2, 25.3, 12.5, 20.2]
        }, {
            name: 'Current Year',
            data: [36.2, 22.4, 38.2, 30.5, 26.4, 30.4, 20.2, 29.6, 10.9, 36.2, 22.4, 38.2]
        }],
        chart: {
            type: 'bar',
            height: 306,
            stacked: true,
            toolbar: {
                show: false,
            }
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '30%',
                borderRadius: 6,
            },
        },
        dataLabels: {
            enabled: false,
        },
        legend: {
            show: true,
            position: 'bottom',
            horizontalAlign: 'center',
            fontWeight: 400,
            fontSize: '8px',
            offsetX: 0,
            offsetY: 0,
            markers: {
                width: 9,
                height: 9,
                radius: 4,
            },
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        grid: {
            show: false,
        },
        colors: colors,
        xaxis: {
          categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
          axisTicks: {
            show: false,
        },
        },
        yaxis: {
          title: {
            text: '$ (thousands)'
          },
        },
        fill: {
          opacity: 1
  
        },
        tooltip: {
          y: {
            formatter: function (val:any) {
              return "$ " + val + " thousands"
            }
          }
        }
      };
     }
  private _basicBarChart(colors:any) {
      colors = this.common.getChartColorsArray(colors);
      this.basicBarChart = {
        series: [{
            data: [1010, 1640, 490, 1255, 1050, 689, 800, 420, 1085, 589],
            name: 'Sessions',
        }],
        chart: {
            type: 'bar',
            height: 436,
            toolbar: {
                show: false,
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 4,
                horizontal: true,
                distributed: true,
                dataLabels: {
                    position: 'top',
                },
            }
        },
        dataLabels: {
            enabled: true,
            offsetX: 32,
            style: {
                fontSize: '12px',
                fontWeight: 400,
                colors: ['#adb5bd']
            }
        },
        colors: colors,
        legend: {
            show: false,
        },
        grid: {
            show: false,
        },
        xaxis: {
            categories: ['India', 'United States', 'China', 'Indonesia', 'Russia', 'Bangladesh', 'Canada', 'Brazil', 'Vietnam', 'UK'],
        },
      };
    }
  private _splineAreaChart(colors:any) {
      colors = this.common.getChartColorsArray(colors);
      this.splineAreaChart = {
        series: [{
            name: 'Revenue',
            data: [20, 25, 30, 35, 40, 55, 70, 110, 150, 180, 210, 250]
        },{
            name: 'Expenses',
            data: [12, 17, 45, 42, 24, 35, 42, 75, 102, 108, 156, 199]
        }],
        chart: {
            height: 290,
            type: 'area',
            toolbar: 'false',
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth',
            width: 2,
        },
        xaxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yaxis: {
            tickAmount: 5,
            min: 0,
            max: 260
        },
        colors: colors,
        fill: {
            opacity: 0.06,
            type: 'solid'
        }
      };
    }
  
  private _DealTypeChart(colors:any) {
      colors = this.common.getChartColorsArray(colors);
      this.DealTypeChart = {
        series: [{
            name: 'Series 1',
            data: [80, 50, 30, 40, 100, 20],
        },
        {
            name: 'Series 2',
            data: [20, 30, 40, 80, 20, 80],
        },
        {
            name: 'Series 3',
            data: [44, 76, 78, 13, 43, 10],
        }
        ],
        chart: {
            height: 350,
            type: 'radar',
            dropShadow: {
                enabled: true, blur: 1, left: 1, top: 1
            },
            toolbar: {
                show: false
            },
        },
        stroke: {
            width: 2
        },
        fill: {
            opacity: 0.2
        },
        markers: {
            size: 0
        },
        colors: colors,
        xaxis: {
            categories: ['2014', '2015', '2016', '2017', '2018', '2019']
        }
      };
    }
  private _salesForecastChart(colors:any) {
      colors = this.common.getChartColorsArray(colors);
      this.salesForecastChart = {
        series: [{
          name: 'Goal',
          data: [37]
        }, {
            name: 'Pending Forcast',
            data: [12]
        }, {
            name: 'Revenue',
            data: [18]
        }],
        chart: {
          type: 'bar',
          height: 341,
          toolbar: {
              show: false,
          },
        },
        plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: '65%',
            },
        },
        stroke: {
            show: true,
            width: 5,
            colors: ['transparent']
        },
        xaxis: {
          categories: [''],
          axisTicks: {
              show: false,
              borderType: 'solid',
              color: '#78909C',
              height: 6,
              offsetX: 0,
              offsetY: 0
          },
          title: {
              text: 'Total Forecasted Value',
              offsetX: 0,
              offsetY: -30,
              style: {
                  color: '#78909C',
                  fontSize: '12px',
                  fontWeight: 400,
              },
          },
        },
        yaxis: {
          labels: {
              formatter: function (value:any) {
                  return "$" + value + "k";
              }
          },
          tickAmount: 4,
          min: 0
        },
        fill: {
            opacity: 1
        },
        legend: {
            show: true,
            position: 'bottom',
            horizontalAlign: 'center',
            fontWeight: 500,
            offsetX: 0,
            offsetY: -14,
            itemMargin: {
                horizontal: 8,
                vertical: 0
            },
            markers: {
                width: 10,
                height: 10,
            }
        },
        colors: colors
      };
    }
    // Chart Colors Set

  FnLoadData() {
      this.dataTotal = [
          {
              title: 'Doanh thu',
              value: 6793049229.1213111,
              icon: 'users',
              persantage: '16.24',
              profit: 'up',
              subtitlelast: ' năm trước'
          }, 
          {
              title: 'Phải thu',
              value: 6793049229,
              icon: 'activity',
              persantage: '3.96',
              profit: 'down',
              subtitlelast: ' năm trước'
          }, 
          {
              title: 'Phải trả',
              value: 6793049229.40,
              icon: 'clock',
              persantage: '0.24',
              profit: 'down',
              subtitlelast: ' năm trước'
          }, 
          {
              title: 'Chi phí',
              value: 6793049229.48,
              icon: 'external-link',
              persantage: '7.05',
              profit: 'up',
              subtitlelast: ' năm trước'
          }, 
          {
              title: 'Tiền mặt',
              value: 6793049229.48,
              icon: 'external-link',
              persantage: '7.05',
              profit: 'up',
              subtitlelast: ' năm trước'
          }, 
          {
              title: 'Dự thu',
              value: 6793049229.48,
              icon: 'external-link',
              persantage: '7.05',
              profit: 'up',
              subtitlelast: ' năm trước'
          }, 
          {
              title: 'Dự chị',
              value: 6793049229.48,
              icon: 'external-link',
              persantage: '7.05',
              profit: 'up',
              subtitlelast: ' năm trước'
          }, 
          {
              title: 'Dự chị',
              value: 6793049229.48,
              icon: 'external-link',
              persantage: '7.05',
              profit: 'up',
              subtitlelast: ' năm trước'
          }
      ];
  }

}
