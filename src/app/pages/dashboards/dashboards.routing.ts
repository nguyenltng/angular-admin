import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../account/guards/auth.guard';
import { CrmComponent } from './crm/crm.component';
import { FinanceComponent } from './finance/finance.component';
import { GeneralComponent } from './general/general.component';
import { HrmComponent } from './hrm/hrm.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { SellComponent } from './sell/sell.component';
import { WarehouseComponent } from './warehouse/warehouse.component';

const routes: Routes = [
    {
        path: "general",
        component: GeneralComponent,canActivate: [AuthGuard]
    },
    {
        path: "finance",
        component: FinanceComponent,canActivate: [AuthGuard]
    },
    {
        path: "sell",
        component: SellComponent,canActivate: [AuthGuard]
    },
    {
        path: "purchase",
        component: PurchaseComponent,canActivate: [AuthGuard]
    },
    {
        path: "warehouse",
        component: WarehouseComponent,canActivate: [AuthGuard]
    },
    {
        path: "crm",
        component: CrmComponent,canActivate: [AuthGuard]
    },
    {
        path: "hrm",
        component: HrmComponent,canActivate: [AuthGuard]
    }
];

export const DashboardsRoutes = RouterModule.forChild(routes);
