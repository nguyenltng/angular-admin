import { Component, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardsRoutes } from './dashboards.routing';
import { GeneralComponent } from './general/general.component';
import { ComponentModule } from 'src/app/shared/component/component.module';
import { NgApexchartsModule } from 'ng-apexcharts';
import { CountToModule } from 'angular-count-to';
import { FinanceComponent } from './finance/finance.component';
import { CrmComponent } from './crm/crm.component';
import { HrmComponent } from './hrm/hrm.component';
import { SellComponent } from './sell/sell.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { WarehouseComponent } from './warehouse/warehouse.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardsRoutes,
    ComponentModule,
    NgApexchartsModule,
    CountToModule
  ],
  declarations: [GeneralComponent,FinanceComponent,CrmComponent,HrmComponent,SellComponent,PurchaseComponent,WarehouseComponent], 
})
export class DashboardsModule { }
