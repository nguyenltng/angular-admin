import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfitRoutes } from './profit.routing';

@NgModule({
  imports: [
    CommonModule,ProfitRoutes
  ],
  declarations: []
})
export class ProfitModule { }
