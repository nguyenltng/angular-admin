export class Connection {
    id?: number;
    username?: string;
    password?: string;
    clientID?: string;
    provider?: string;
    clientSecret?: string;
    privateToken?: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}