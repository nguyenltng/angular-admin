export class Customer{
    id?: number;
    firstName?: string;
    lastName?: string;
    gender?: string;
    birthDate?: string;
    defaultAddress?: string;
    phone?: string;
    email?: string;
    state?: string;
    note?: string;
    tags?: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}