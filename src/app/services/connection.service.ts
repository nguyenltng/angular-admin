import { Injectable } from '@angular/core';
import { of, switchMap } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { LogService } from 'src/app/services/log.service';
import { environment } from 'src/environments/environment';
import { Connection } from '../model/connection.model';

@Injectable({
    providedIn: 'root'
})
export class ConnectionService {
    UrlApi : string = environment.UrlApi || "";

    constructor(private logService: LogService,private apiService: ApiService) {
     }

    getAllConnections() {
        return this.apiService.get(this.UrlApi +"connection").pipe(
            switchMap((response:any)=>{
                return of(response);
            }),
        )
    }
    addConnection(connection : Connection) {
        return this.apiService.post(this.UrlApi +"connection",connection).pipe(
            switchMap((response:any)=>{
                return of(response);
            }),
        )
    }
    updateConnection(id: number, connection: Connection){
        return this.apiService.put(this.UrlApi +"connection/"+id ,connection).pipe(
            switchMap((response:any)=>{
                return of(response);
            }),
        )
    }
   
}
