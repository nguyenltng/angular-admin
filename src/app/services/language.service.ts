import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { LogService } from './log.service';

@Injectable({ providedIn: 'root' })
export class LanguageService {
  location:string = "Services.LanguageService";
  public languages: string[] = ['vi','en' ];
  constructor(public translate: TranslateService, private cookieService: CookieService, private logService: LogService) {

    let browserLang: any;
    /***
     * cookie Language Get
    */
    this.translate.addLangs(this.languages);
    if (this.cookieService.check('lang')) {
      browserLang = this.cookieService.get('lang');
    }
    else {
      browserLang = translate.getBrowserLang();
    }
    translate.use(browserLang.match(/vi|en/) ? browserLang : 'vi');
    this.translate.setDefaultLang(browserLang.match(/vi|en/) ? browserLang : 'vi');
  }

  /***
   * Cookie Language set
   */
  public  setLanguage(lang: any) {
    this.cookieService.set('lang', lang);
    this.logService.log(lang,this.location);
    this.logService.log(this.translate.langs,this.location);
    try {
       this.translate.use('en');
       this.translate.setDefaultLang(lang);
    } catch (err) {
      console.log(err);
    }
    console.log(`Successfully initialized ${lang} language.`);
    this.logService.log(this.translate.currentLang,this.location);
  }

}
