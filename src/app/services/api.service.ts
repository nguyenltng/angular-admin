import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Params } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../pages/account/auth.service';

@Injectable({
   providedIn: 'root'
})
export class ApiService {
   constructor(private http: HttpClient, private authService: AuthService) { 
   }

   private getHeaders(): HttpHeaders {
      // Get the access token from the AuthService
      let struser:string = localStorage.getItem('user') ?? '';
      let user =  JSON.parse(struser);
      if(user.token){
         return new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': `Bearer ${user.token}`
          });
      }
      return new HttpHeaders({
         'Content-Type': 'application/json; charset=utf-8',
       });
    }

   public get<T>(url: string, routerParams?: Params, routerHeaders?: HttpHeaders): Observable<T> {
      let queryParams: Params = {};
      if (routerParams) {
         queryParams = this.setParameter(routerParams);
      }
      let headersOri = this.getHeaders();
      if (routerHeaders) {
         if(!routerHeaders.has("Content-Type"))
            routerHeaders.set("Content-Type", "application/json; charset=utf-8");
         headersOri = routerHeaders;
      }
      return this.http.get<T>(url, { params: queryParams,headers: headersOri});
   }

   public put<T>(url: string, body: Record<string, any> = {}): Observable<any> {
      let headersOri = this.getHeaders();
      return this.http.put(url, body, {headers: headersOri});
   }

   public post<T>(url: string, body: Record<string, any> = {}, routerHeaders?: HttpHeaders): Observable<any> {
      let headersOri = this.getHeaders();
      if (routerHeaders) {
         if(!routerHeaders.has("Content-Type"))
            routerHeaders.set("Content-Type", "application/json; charset=utf-8");
         headersOri = routerHeaders;
      }
      return this.http.post(url, body,{headers: headersOri});
   }

   public delete<T>(url: string): Observable<any> {
      let headersOri = this.getHeaders();
      return this.http.delete(url,{headers: headersOri});
   }
   
   private setParameter(routerParams: Params): HttpParams {
      let queryParams = new HttpParams();
      for (const key in routerParams) {
         if (routerParams.hasOwnProperty(key)) {
            queryParams = queryParams.set(key, routerParams[key]);
         }
      }
      return queryParams;
   }
}
