import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LogService {
    log(message:any,location?:string) {
        location = location || "";
        if (environment.log) {
            console.log(message,location);
        }
    }
    constructor() { }
}
