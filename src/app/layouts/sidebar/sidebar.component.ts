import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { LogService } from 'src/app/services/log.service';
import { MENU } from './menu';
import { MenuItem } from './menu.model';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    location: string = 'sidebar.component.ts';
    menu: any;
    toggle: any = true;
    menuItems: MenuItem[] = [];
    @ViewChild('sideMenu') sideMenu!: ElementRef;
    @Output() mobileMenuButtonClicked = new EventEmitter();
    constructor(private router: Router, public translate: TranslateService, private logService: LogService,private cookie: CookieService) {
        logService.log(translate.currentLang,this.location + "constructor");
    }

    ngOnInit() {
        // Menu Items
        this.menuItems = MENU;
    }
    ngAfterViewInit(){
        this.initActiveMenu();
    }
    removeActivation(items: any) {
        items.forEach((item: any) => {
            if (item.classList.contains("menu-link")) {
                if (!item.classList.contains("active")) {
                    item.setAttribute("aria-expanded", false);
                }
                (item.nextElementSibling) ? item.nextElementSibling.classList.remove("show") : null;
            }
            if (item.classList.contains("nav-link")) {
                if (item.nextElementSibling) {
                    item.nextElementSibling.classList.remove("show");
                }
                item.setAttribute("aria-expanded", false);
            }
            item.classList.remove("active");
        });
    }
    activateParentDropdown(item: any) { // navbar-nav menu add active
        item.classList.add("active");
        let parentCollapseDiv = item.closest(".collapse.menu-dropdown");
        if (parentCollapseDiv) {
            //   // to set aria expand true remaining
            parentCollapseDiv.classList.add("show");
            parentCollapseDiv.parentElement.children[0].classList.add("active");
            parentCollapseDiv.parentElement.children[0].setAttribute("aria-expanded", "true");
            if (parentCollapseDiv.parentElement.closest(".collapse.menu-dropdown")) {
                parentCollapseDiv.parentElement.closest(".collapse").classList.add("show");
                if (parentCollapseDiv.parentElement.closest(".collapse").previousElementSibling)
                    parentCollapseDiv.parentElement.closest(".collapse").previousElementSibling.classList.add("active");
            }
            return false;
        }
        return false;
    }
    updateActive(event: any) {
        const ul = document.getElementById("navbar-nav");
        if (ul) {
          const items = Array.from(ul.querySelectorAll("a.nav-link"));
          this.removeActivation(items);
        }
        this.activateParentDropdown(event.target);
    }
    initActiveMenu() {
        const pathName = window.location.pathname;
        const ul = document.getElementById("navbar-nav");
        console.log(ul);
        if (ul) {
            const items = Array.from(ul.querySelectorAll("a.nav-link"));
            let activeItems = items.filter((x: any) => x.classList.contains("active"));
            console.log(activeItems);
            this.removeActivation(activeItems);

            let matchingMenuItem = items.find((x: any) => {
                return x.pathname === pathName;
            });
            if (matchingMenuItem) {
                this.activateParentDropdown(matchingMenuItem);
            }
        }
    }
    hasItems(item: MenuItem) {
        return item.subItems !== undefined ? item.subItems.length > 0 : false;
    }
    toggleSubItem(event: any) {
        if (event.target && event.target.nextElementSibling)
          event.target.nextElementSibling.classList.toggle("show");
    };
    toggleItem(event: any) {
        let isCurrentMenuId = event.target.closest('a.nav-link');
        let isMenu = isCurrentMenuId.nextElementSibling as any;
        if (isMenu.classList.contains("show")) {
          isMenu.classList.remove("show");
          isCurrentMenuId.setAttribute("aria-expanded", "false");
        } else {
          let dropDowns = Array.from(document.querySelectorAll('#navbar-nav .show'));
          dropDowns.forEach((node: any) => {
            node.classList.remove('show');
          });
          (isMenu) ? isMenu.classList.add('show') : null;
          const ul = document.getElementById("navbar-nav");
          if (ul) {
            const iconItems = Array.from(ul.getElementsByTagName("a"));
            let activeIconItems = iconItems.filter((x: any) => x.classList.contains("active"));
            activeIconItems.forEach((item: any) => {
              item.setAttribute('aria-expanded', "false")
              item.classList.remove("active");
            });
          }
          isCurrentMenuId.setAttribute("aria-expanded", "true");
          if (isCurrentMenuId) {
            this.activateParentDropdown(isCurrentMenuId);
          }
        }
    }
    toggleMobileMenu(event: any) {
        var sidebarsize = document.documentElement.getAttribute("data-sidebar-size");
        if (sidebarsize == 'sm-hover-active') {
          document.documentElement.setAttribute("data-sidebar-size", 'sm-hover')
        } else {
          document.documentElement.setAttribute("data-sidebar-size", 'sm-hover-active')
        }
    }

    SidebarHide() {
        document.body.classList.remove('vertical-sidebar-enable');
    }
    
}
