import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
  {
    id: 1,
    label: 'MENUITEMS.MENU.TEXT',
    isTitle: true
  },
  {
    id: 2,
    label: 'MENUITEMS.DASHBOARD.TEXT',
    icon: 'ri-dashboard-2-line',
    link: '',
  },
  {
    id: 3,
    label: 'MENUITEMS.ORDER.TEXT',
    link: '/order',
    icon: 'ri-line-chart-line',
  },
  {
    id: 4,
    label: 'MENUITEMS.CUSTOMER.TEXT',
    link: '/customer',
    icon: 'ri-user-5-line',
  },
  {
    id: 5,
    label: 'MENUITEMS.SETTING.TEXT',
    link: '/setting',
    icon: 'ri-settings-3-line',
    subItems: [
      {
        id: 6,
        label: 'MENUITEMS.SETTING.LIST.CONNECTION',
        link: '/setting/connection',
        parentId: 5
      },
    
    ]
  },


  // {
  //   id: 4,
  //   label: 'MENUITEMS.DASHBOARD.LIST.FINANCE',
  //   link: '/dashboard/finance',
  //   parentId: 2
  // },
  // {
  //   id: 5,
  //   label: 'MENUITEMS.DASHBOARD.LIST.SELL',
  //   link: '/dashboard/sell',
  //   parentId: 2
  // },
  // {
  //   id: 6,
  //   label: 'MENUITEMS.DASHBOARD.LIST.PURCHASE',
  //   link: '/dashboard/purchase',
  //   parentId: 2
  // },
  // {
  //   id: 7,
  //   label: 'MENUITEMS.DASHBOARD.LIST.WAREHOUSE',
  //   link: '/dashboard/warehouse',
  //   parentId: 2
  // },
  // {
  //   id: 8,
  //   label: 'MENUITEMS.DASHBOARD.LIST.CRM',
  //   link: '/dashboard/crm',
  //   parentId: 2
  // },
  // {
  //   id: 9,
  //   label: 'MENUITEMS.DASHBOARD.LIST.HRM',
  //   link: '/dashboard/hrm',
  //   parentId: 2
  // }
  //   ]
  // },
  {
    id: 10,
    label: 'MENUITEMS.REVENUEANALYSIS.TEXT',
    icon: 'ri-line-chart-line',
    subItems: [
      {
        id: 11,
        label: 'MENUITEMS.REVENUEANALYSIS.LIST.LOCATION',
        link: '/analytics/location',
        parentId: 10
      },
      {
        id: 12,
        label: 'MENUITEMS.REVENUEANALYSIS.LIST.BRANCH',
        link: '/analytics/branch',
        parentId: 10
      },
      {
        id: 13,
        label: 'MENUITEMS.REVENUEANALYSIS.LIST.TEAM',
        link: '/analytics/team',
        parentId: 10
      },
      {
        id: 14,
        label: 'MENUITEMS.REVENUEANALYSIS.LIST.PRODUCT',
        link: '/analytics/product',
        parentId: 10,

      },
      {
        id: 15,
        label: 'MENUITEMS.REVENUEANALYSIS.LIST.CUSTOMER',
        link: '/analytics/customer',
        parentId: 10,

      },
      {
        id: 16,
        label: 'MENUITEMS.REVENUEANALYSIS.LIST.PARTNER',
        link: '/analytics/partner',
        parentId: 10,

      },
      {
        id: 17,
        label: 'MENUITEMS.REVENUEANALYSIS.LIST.TIME',
        link: '/analytics/time',
        parentId: 10,

      }
    ]
  },
  // {
  //   id: 18,
  //   label: 'MENUITEMS.COST.TEXT',
  //   icon: 'ri-exchange-dollar-line',
  //   subItems: [
  //     {
  //       id: 19,
  //       label: 'MENUITEMS.COST.LIST.LOCATION',
  //       link: '/cost/location',
  //       parentId: 18
  //     },
  //     {
  //       id: 20,
  //       label: 'MENUITEMS.COST.LIST.BRANCH',
  //       link: '/cost/branch',
  //       parentId: 18
  //     },
  //     {
  //       id: 21,
  //       label: 'MENUITEMS.COST.LIST.DEPARTMENT',
  //       link: '/cost/department',
  //       parentId: 18
  //     },
  //     {
  //       id: 22,
  //       label: 'MENUITEMS.COST.LIST.ITEMS',
  //       link: '/cost/items',
  //       parentId: 18,

  //     }
  //   ]
  // },
  // {
  //   id: 23,
  //   label: 'MENUITEMS.PROFIT.TEXT',
  //   icon: 'ri-exchange-dollar-line',
  //   subItems: [
  //     {
  //       id: 24,
  //       label: 'MENUITEMS.PROFIT.LIST.LOCATION',
  //       link: '/profit/location',
  //       parentId: 23
  //     },
  //     {
  //       id: 25,
  //       label: 'MENUITEMS.PROFIT.LIST.BRANCH',
  //       link: '/profit/branch',
  //       parentId: 23
  //     },
  //     {
  //       id: 26,
  //       label: 'MENUITEMS.PROFIT.LIST.DEPARTMENT',
  //       link: '/profit/department',
  //       parentId: 23
  //     },
  //     {
  //       id: 27,
  //       label: 'MENUITEMS.PROFIT.LIST.PRODUCT',
  //       link: '/profit/product',
  //       parentId: 23,

  //     },
  //     {
  //       id: 28,
  //       label: 'MENUITEMS.PROFIT.LIST.CUSTOMER',
  //       link: '/profit/customer',
  //       parentId: 23,

  //     },
  //     {
  //       id: 29,
  //       label: 'MENUITEMS.PROFIT.LIST.PARTNER',
  //       link: '/profit/partner',
  //       parentId: 23,

  //     }
  //   ]
  // }

];
