import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutsComponent } from './layouts.component';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SimplebarAngularModule, } from 'simplebar-angular';
import { TranslateModule } from '@ngx-translate/core';
import { LanguageService } from '../services/language.service';
import { TopbarComponent } from './topbar/topbar.component';
import { NgbDropdownModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  imports: [
    CommonModule,RouterModule,SimplebarAngularModule,TranslateModule,NgbNavModule,NgbDropdownModule,DataTablesModule
  ],
  declarations: [LayoutsComponent,TopbarComponent,SidebarComponent],

  providers:  []
})
export class LayoutsModule { }
