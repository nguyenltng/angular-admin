import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from 'src/app/pages/account/auth.service';
import { LanguageService } from 'src/app/services/language.service';
import { environment } from 'src/environments/environment';
declare var $: any;
@Component({
    selector: 'app-topbar',
    templateUrl: './topbar.component.html',
    styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {
    location:string = './topbar.component.ts';
    element: any;
    mode: string | undefined;
    @Output() mobileMenuButtonClicked = new EventEmitter();

    flagvalue: any;
    countryName: any;
    cookieValue: any;
    tableConfig: DataTables.Settings = {};
    constructor(@Inject(DOCUMENT) private document: any, public languageService: LanguageService,
        public _cookiesService: CookieService, private router: Router, private authService: AuthService) { }

    ngOnInit(): void {
        this.element = document.documentElement;

        // Cookies wise Language set
        this.cookieValue = this._cookiesService.get('lang');
        const val = this.listLang.filter(x => x.lang === this.cookieValue);
        this.countryName = val.map(element => element.text);
        if (val.length === 0) {
            this.flagvalue = 'assets/images/flags/vi.svg';
            this.languageService.setLanguage('vi');
        } else {
            this.flagvalue = val.map(element => element.flag);
        }
    }

    /**
     * Toggle the menu bar when having mobile screen
     */
    toggleMobileMenu(event: any) {
        event.preventDefault();
        this.mobileMenuButtonClicked.emit();
        setTimeout(() => {
            let table = $('body').find('table.dataTable').DataTable();
            if (table.context.length > 0) {
                table.columns.adjust();
            }
        }, 200);
    }

    /**
     * Fullscreen method
     */
    fullscreen() {
        document.body.classList.toggle('fullscreen-enable');
        if (
            !document.fullscreenElement && !this.element.mozFullScreenElement &&
            !this.element.webkitFullscreenElement) {
            if (this.element.requestFullscreen) {
                this.element.requestFullscreen();
            } else if (this.element.mozRequestFullScreen) {
                /* Firefox */
                this.element.mozRequestFullScreen();
            } else if (this.element.webkitRequestFullscreen) {
                /* Chrome, Safari and Opera */
                this.element.webkitRequestFullscreen();
            } else if (this.element.msRequestFullscreen) {
                /* IE/Edge */
                this.element.msRequestFullscreen();
            }
        } else {
            if (this.document.exitFullscreen) {
                this.document.exitFullscreen();
            } else if (this.document.mozCancelFullScreen) {
                /* Firefox */
                this.document.mozCancelFullScreen();
            } else if (this.document.webkitExitFullscreen) {
                /* Chrome, Safari and Opera */
                this.document.webkitExitFullscreen();
            } else if (this.document.msExitFullscreen) {
                /* IE/Edge */
                this.document.msExitFullscreen();
            }
        }
    }

    /**
    * Topbar Light-Dark Mode Change
    */
    changeMode(mode: string) {
        this.mode = mode;
        // this.eventService.broadcast('changeMode', mode);

        switch (mode) {
            case 'light':
                document.body.setAttribute('data-layout-mode', "light");
                document.body.setAttribute('data-sidebar', "light");
                // document.documentElement.setAttribute('data-sidebar', 'light');
                // document.documentElement.setAttribute('data-layout-mode', 'light');
                break;
            case 'dark':
                document.body.setAttribute('data-layout-mode', "dark");
                document.body.setAttribute('data-sidebar', "dark");
                // document.documentElement.setAttribute('data-sidebar', 'dark');
                // document.documentElement.setAttribute('data-layout-mode', 'dark');
                break;
            default:
                document.body.setAttribute('data-layout-mode', "light");
                // document.documentElement.setAttribute('data-sidebar', 'dark');
                break;
        }
    }

    /***
     * Language Listing
     */
    listLang = [
        { text: 'Tiếng việt', flag: 'assets/images/flags/vi.svg', lang: 'vi' },
        { text: 'English', flag: 'assets/images/flags/us.svg', lang: 'en' },
    ];

    /***
     * Language Value Set
     */
    setLanguage(text: string, lang: string, flag: string) {
        this.countryName = text;
        this.flagvalue = flag;
        this.cookieValue = lang;
        this.languageService.setLanguage(lang);
    }

    // Get user

    user = this.authService.userLogin;
    /**
     * Logout the user
     */
    logout() {
        this.authService.logout();
        this.router.navigate(['/auth/login']);
    }


}
