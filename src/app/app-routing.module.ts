import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsComponent } from './layouts/layouts.component';
import { AuthGuard } from './pages/account/guards/auth.guard';
import { OrderComponent } from './pages/order/order.component';

const routes: Routes = [
  {
    path: '', 
    component: LayoutsComponent, 
    loadChildren: () => import('./pages/pages.module')
      .then(m => m.PagesModule), 
    canActivate: [AuthGuard]
  },
  { path: 'auth', loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule],

})
export class AppRoutingModule { }
