import { Pipe, PipeTransform, Injectable } from '@angular/core';
 
@Injectable()
@Pipe({ name: 'monypipe' })
export class MoneyPipe implements PipeTransform {
  transform(value: number,  decimalDelimiter: string = ".", thousandsDelimiter: string | null = "," ,numberDelimiter: number | 0 = 0,symbol: string = "",): any {
    if(!value) return symbol ? symbol + " -" : "";
 
    let roundValue = value.toFixed(Math.max(0, numberDelimiter));
    let valueToReturn = roundValue.replace('.', decimalDelimiter);
    if(thousandsDelimiter) valueToReturn = this.setThousandsSign(valueToReturn, thousandsDelimiter,decimalDelimiter);
    return symbol ? symbol + " " + valueToReturn : valueToReturn;
  }
 
  private setThousandsSign(value: string, delimiter: string,decimalDelimiter: string): string {
    let arrayValue = value.split(decimalDelimiter);
    let decimal = arrayValue.length > 1 ? decimalDelimiter+arrayValue[1] : "";
    return arrayValue[0].split("").reverse().map((str, index) => {
      if(index < 3) return str;
 
      return (index%3)===0 ? str+delimiter : str;
    }).reverse().join("") + decimal;
  }
}