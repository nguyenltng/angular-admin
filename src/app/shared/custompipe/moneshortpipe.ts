import { Pipe, PipeTransform, Injectable } from '@angular/core';
 
@Injectable()
@Pipe({ name: 'monyshortpipe' })
export class MoneyShortPipe implements PipeTransform {
  transform(value: number, numberDelimiter: number | 0 = 2,): any {
    let valueToReturn = this.convertNumbertoStringShort(value,numberDelimiter);
    return valueToReturn;
  }
 
  private setThousandsSign(value: string): string {
    let arrayValue = value.split(".");
    let decimal = arrayValue.length > 1 ? "." + arrayValue[1] : "";
    return arrayValue[0].split("").reverse().map((str, index) => {
      if(index < 3) return str;
 
      return (index%3)===0 ? str+"," : str;
    }).reverse().join("") + decimal;
  }
  private convertNumbertoStringShort(valueBF:number,numberDelimiter:number):string {
    var valueReturn:string =  ""; 
      if (valueBF >= 1000000000) {
        let number = valueBF / 1000000000;
        valueReturn = this.setThousandsSign(number.toFixed(numberDelimiter)) + " B"
      }
      else if (valueBF >= 1000000) {
        let number = valueBF / 1000000;
        valueReturn = this.setThousandsSign(number.toFixed(numberDelimiter)) + " M"
      } else if (valueBF >= 1000) {
        let number = valueBF / 1000;
        valueReturn = this.setThousandsSign(number.toFixed(numberDelimiter)) + " K";
      }
      if (valueBF == 0) {
        valueReturn = " 0";
      }
      return valueReturn;
}
}