import 'datatables.net-buttons/js/buttons.flash';
declare var $: any;
export class Common {
    getChartColorsArray = (colors: any) => {
        colors = JSON.parse(colors);
        return colors.map(function (value: any) {
            var newValue = value.replace(" ", "");
            if (newValue.indexOf(",") === -1) {
                var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
                if (color) {
                    color = color.replace(" ", "");
                    return color;
                }
                else return newValue;;
            } else {
                var val = value.split(',');
                if (val.length == 2) {
                    var rgbaColor = getComputedStyle(document.documentElement).getPropertyValue(val[0]);
                    rgbaColor = "rgba(" + rgbaColor + "," + val[1] + ")";
                    return rgbaColor;
                } else {
                    return newValue;
                }
            }
        });
    }
    // SetHeightContain = (id:string, options?:number | 0) => {
    //     try {
    //         let _add = options || 0;
    //         let _s = $('#' + id);
    //         let _o = _s.offset();
    //         let _h = $(window).height();
    //         return (_h - _o.top - _add)

    //     } catch (error) {
    //         console.log(JSON.stringify(error));
    //         return 500;
    //     }
    // }

    configTableDefault = (id: string, title?: string, height?: number) => {
        return ({
            responsive: true,
            retrieve: true,
            paging: false,
            search: false,
            info: false,
            searching: true,
            scrollX: true,
            scrollCollapse: true,
            language: {
                search: 'Search',
            },
            aoColumnDefs: [{
                bSortable: false,
                aTargets: 'no-sort'
            }],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copy',
                    text: 'Copy to clipboard',
                    className: 'btn btn-success btn-sm',
                    init: function (api: any, node: any, config: any) {
                        $(node).removeClass('dt-button')
                    }
                },
                {
                    extend: 'excel',
                    className: 'btn btn-primary btn-sm',
                    title: title,
                    init: function (api: any, node: any, config: any) {
                        $(node).removeClass('dt-button')
                    }
                },
                ,
                {
                    extend: 'pdf',
                    className: 'btn btn-primary btn-sm',
                    title: title,
                    init: function (api: any, node: any, config: any) {
                        $(node).removeClass('dt-button')
                    }
                },
                {
                    extend: 'print',
                    className: 'btn btn-secondary btn-sm',
                    title: title,
                    init: function (api: any, node: any, config: any) {
                        $(node).removeClass('dt-button')
                    }
                },
                {
                    text: "Save Template",
                    className: 'btn btn-secondary btn-sm',
                    title: title,
                    init: function (api: any, node: any, config: any) {
                        $(node).removeClass('dt-button')
                    }
                },
                {
                    text: "Save Result",
                    className: 'btn btn-secondary btn-sm',
                    title: title,
                    init: function (api: any, node: any, config: any) {
                        $(node).removeClass('dt-button')
                    }
                },
                // {
                //     text: 'My button',
                //     className: 'btn btn-secondary btn-sm',
                //     action: function (e: any, dt: any, node: any, config: any) {
                //         alert('Button activated');
                //     },
                //     init: function (api: any, node: any, config: any) {
                //         $(node).removeClass('dt-button')
                //     }
                // }
            ],
        })

    }

    SetTable = (id: string, title?: string, options: number = 0) => {
        let table = $("#" + id).DataTable(this.configTableDefault(id, title));
        setTimeout(function () {
            try {
                var offset = $('.dataTables_scrollBody').offset();
                var top = offset.top;
                let _height = $(window).height();
                $('.dataTables_scrollBody').css('height', _height - top - options);
                $('.dataTables_scrollBody').css('max-height', _height - top - options);
                $('.dataTables_scrollBody').css('min-height', _height - top - options < 300 ? 300 : _height - top - options);
                table.columns.adjust().draw();
                $("body").find(".dt-buttons").addClass('d-flex flex-wrap gap-2 mb-2')
            } catch (error) {
            }
        }, 200);
    }
}
