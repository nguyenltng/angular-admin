/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Totalcount5Component } from './totalcount5.component';

describe('Totalcount5Component', () => {
  let component: Totalcount5Component;
  let fixture: ComponentFixture<Totalcount5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Totalcount5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Totalcount5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
