import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-totalcount5',
  templateUrl: './totalcount5.component.html',
  styleUrls: ['./totalcount5.component.css']
})
export class Totalcount5Component implements OnInit {
  @Input() title: string | undefined;
  @Input() value: any | undefined;
  @Input() icon: string | undefined;
  @Input() persantage: string | undefined;
  @Input() profit: string | undefined;
  @Input() month: string | undefined;
  constructor() { }

  ngOnInit() {
  }

}
