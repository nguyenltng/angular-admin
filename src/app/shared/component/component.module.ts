import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { Totalcount1Component } from './totalcount1/totalcount1.component';
import { CountToModule } from 'angular-count-to';
import { Totalcount2Component } from './totalcount2/totalcount2.component';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
import { Totalcount3Component } from './totalcount3/totalcount3.component';
import { Totalcount4Component } from './totalcount4/totalcount4.component';
import { Totalcount5Component } from './totalcount5/totalcount5.component';
import { MoneyPipe } from '../custompipe/moneypipe';
import { MoneyShortPipe } from '../custompipe/moneshortpipe';



@NgModule({
  imports: [
    CommonModule,
    CountToModule,
    FeatherModule.pick(allIcons),
  ],
  declarations: [BreadcrumbsComponent,Totalcount1Component,Totalcount2Component,Totalcount3Component,Totalcount4Component,Totalcount5Component,MoneyPipe,MoneyShortPipe],
  exports:[BreadcrumbsComponent,Totalcount1Component,Totalcount2Component,Totalcount3Component,Totalcount4Component,Totalcount5Component]
})
export class ComponentModule { }
