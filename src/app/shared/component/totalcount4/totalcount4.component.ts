import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-totalcount4',
  templateUrl: './totalcount4.component.html',
  styleUrls: ['./totalcount4.component.css']
})
export class Totalcount4Component implements OnInit {

  @Input() title: string | undefined;
  @Input() value: any | undefined;
  @Input() icon: string | undefined;
  @Input() persantage: string | undefined;
  @Input() profit: string | undefined;
  @Input() titlelink: string | undefined;
  @Input() link: string | undefined;
  constructor() { }

  ngOnInit() {
  }

}
