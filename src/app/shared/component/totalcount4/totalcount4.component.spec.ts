/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Totalcount4Component } from './totalcount4.component';

describe('Totalcount4Component', () => {
  let component: Totalcount4Component;
  let fixture: ComponentFixture<Totalcount4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Totalcount4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Totalcount4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
