/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Totalcount1Component } from './totalcount1.component';

describe('Totalcount1Component', () => {
  let component: Totalcount1Component;
  let fixture: ComponentFixture<Totalcount1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Totalcount1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Totalcount1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
