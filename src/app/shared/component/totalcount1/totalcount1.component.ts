import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-totalcount1',
  templateUrl: './totalcount1.component.html',
  styleUrls: ['./totalcount1.component.css']
})
export class Totalcount1Component implements OnInit {

  @Input() title: string | undefined;
  @Input() value: any | undefined;
  @Input() icon: string | undefined;
  @Input() profit: string | undefined;
  constructor() { }

  ngOnInit() {
  }

}
