import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-totalcount3',
  templateUrl: './totalcount3.component.html',
  styleUrls: ['./totalcount3.component.css']
})
export class Totalcount3Component implements OnInit {
  @Input() title: string | undefined;
  @Input() value: any | undefined;
  @Input() icon: string | undefined;
  @Input() persantage: string | undefined;
  @Input() profit: string | undefined;
  constructor() { }

  ngOnInit() {
  }

}
