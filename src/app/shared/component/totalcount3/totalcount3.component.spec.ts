/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Totalcount3Component } from './totalcount3.component';

describe('Totalcount3Component', () => {
  let component: Totalcount3Component;
  let fixture: ComponentFixture<Totalcount3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Totalcount3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Totalcount3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
