import { Component, Input, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-totalcount2',
  templateUrl: './totalcount2.component.html',
  styleUrls: ['./totalcount2.component.css']
})
export class Totalcount2Component implements OnInit {
  @Input() title: string | undefined;
  @Input() subtitlelast: string | undefined;
  @Input() value: any | undefined;
  @Input() icon: string | undefined;
  @Input() persantage: string | undefined;
  @Input() profit: string | undefined;
  flag:boolean = false;
  constructor() { }

  ngOnInit() {
   
  }
  ngAfterViewInit(){
    setTimeout(() => {
      let a = $('body').find(".counter-value").hide();
      this.flag = true; 
    }, 1000);
  }

}
