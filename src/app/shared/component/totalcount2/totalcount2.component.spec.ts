/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Totalcount2Component } from './totalcount2.component';

describe('Totalcount2Component', () => {
  let component: Totalcount2Component;
  let fixture: ComponentFixture<Totalcount2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Totalcount2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Totalcount2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
